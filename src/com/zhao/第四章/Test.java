package com.zhao.第四章;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-08 15:35
 **/
public class Test {

    public Test(Integer testValue) {
        this.testValue = testValue;
    }

    private Integer testValue;

    public Integer getTestValue() {
        return testValue;
    }

    public void setTestValue(Integer testValue) {
        this.testValue = testValue;
    }

    @Override
    public String toString() {
        return "Test{" +
                "testValue=" + testValue +
                '}';
    }
}
