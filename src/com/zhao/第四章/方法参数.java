package com.zhao.第四章;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-08 15:16
 **/
public class 方法参数 {

    public static void main(String[] args) {
//        基本类型参数测试();
//        包装类型参数测试();
//        引用对象参数测试();
        对象引用交换();
    }

    private static void 基本类型参数测试() {
        int i = 1;
        intTest(i);
        System.out.println(i);
    }

    private static void intTest(int i) {
        i = i + 1;
    }

    private static void 包装类型参数测试() {
        String str = "a";
        stringTest(str);
        System.out.println(str);
        Integer i = 1;
        integerTest(i);
        System.out.println(i);
    }

    private static void stringTest(String str) {
        str = str + "b";
    }

    private static void integerTest(Integer i) {
        i = i + 1;
    }

    private static void 引用对象参数测试() {
        Test test = new Test(1);
        testTest(test);
        System.out.println(test.toString());
    }

    private static void testTest(Test test) {
        test.setTestValue(2);
    }

    private static void 对象引用交换() {
        Test test1 = new Test(1);
        Test test2 = new Test(2);
        对象交换Test(test1, test2);
        System.out.println(test1);
        System.out.println(test2);
    }

    private static void 对象交换Test(Test test1, Test test2) {
        Test temp = test1;
        test1 = test2;
        test2 = temp;
    }
}
