package com.zhao.第三章;

public class 字符串 {


    public static void main(String[] args) throws Exception {
//        码点与代码单元();
//        charAt和codePointAt比较();
//        遍历字符串();
//        码点转字符串();
//        字符串转码点数组();
//        equals测试();
//        indexOf测试();
        repeat测试();
    }

    private static void 码点与代码单元() {
        char[] chs = Character.toChars(0x10400);
        System.out.printf("U+10400 高代理字符: %04x%n", (int)chs[0]);
        System.out.printf("U+10400 低代理字符: %04x%n", (int)chs[1]);
        System.out.println("U+10400 高代理字符: " + (int)chs[0]);
        System.out.println("U+10400 低代理字符: " + (int)chs[1]);
        String str = new String(chs);
        System.out.println("字符串str: " + str);
        System.out.println("代码单元长度: " + str.length());
        System.out.println("代码点数量: " + str.codePointCount(0, str.length()));
        char first = str.charAt(0);
        char last = str.charAt(1);
        System.out.println(first);
        System.out.println(last);
    }

    private static void charAt和codePointAt比较() {
        String str = "abcd";
        System.out.println(str.charAt(0));
        System.out.println(str.charAt(1));
        System.out.println(str.charAt(2));
        System.out.println(str.charAt(3));

        System.out.println(str.codePointAt(0));
        System.out.println(str.codePointAt(1));
        System.out.println(str.codePointAt(2));
        System.out.println(str.codePointAt(3));

        char[] chs = Character.toChars(0x10400);
        String str1 = new String(chs);
        System.out.println("字符串str1: " + str1);

        System.out.println(str1.charAt(0));
        System.out.println(str1.charAt(1));

        System.out.println(str1.codePointAt(0));
        System.out.println(str1.codePointAt(1));
    }

    private static void 遍历字符串() {
        char[] chs = Character.toChars(0x1D546);
        String str = new String(chs);
        System.out.println(str.codePointCount(0, str.length()));
        System.out.println(str.length());
        str = str + str;
        System.out.println(str.codePointCount(0, str.length()));
        System.out.println(str.length());
        for (int i = 0; i < str.codePointCount(0, str.length()); i++) {
            int cp = str.codePointAt(i);
            System.out.println(cp);
        }
    }

    private static void 码点转字符串() {
        int [] codePoints1 = new int[] { 120134 };
        String str1 = new String(codePoints1, 0, codePoints1.length);
        System.out.println(str1);
        int [] codePoints2 = new int[] { 56646 };
        String str2 = new String(codePoints2, 0, codePoints1.length);
        System.out.println(str2);
    }

    private static void 字符串转码点数组() {
        char[] chs = Character.toChars(0x1D546);
        String str = new String(chs);
        str = str + str;
        System.out.println(str);
        System.out.println("str:codePointCount()" + str.codePointCount(0, str.length()));

        System.out.println("str:length()" + str.length());

        int [] codePoints = str.codePoints().toArray();

        System.out.println(str.charAt(0));
        System.out.println(str.charAt(1));
        System.out.println(str.codePointAt(0));
        System.out.println(str.codePointAt(1));
        System.out.println(str.codePointAt(2));
        System.out.println(str.codePointAt(3));

        for (int i = 0; i < codePoints.length; i++) {
            int [] temp = new int [] { codePoints[i] };
            System.out.println(new String(temp, 0 , 1));
        }
        System.out.println(str.offsetByCodePoints(0, 1));
    }

    private static void equals测试() {
        char[] chs = Character.toChars(0x1D546);
        String str = new String(chs);
        String str1 = str + "123" + str;
        System.out.println(str1.startsWith(str));
        System.out.println(str1.endsWith(str));
    }

    private static void indexOf测试() {
        String str = "1234567";
        System.out.println(str.indexOf("12"));
        System.out.println(str.indexOf("2"));
        System.out.println(str.indexOf("23"));
        System.out.println(str.indexOf("2 "));
        System.out.println(str.indexOf("24"));

        System.out.println("-------------------------");

        System.out.println(str.indexOf("5", 3));
        System.out.println(str.indexOf("5", 4));
        System.out.println(str.indexOf("5", 5));
    }

    private static void repeat测试() {
        String str = "a";
//        System.out.println(str.repeat(5));
    }
}
