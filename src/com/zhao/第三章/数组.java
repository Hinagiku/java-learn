package com.zhao.第三章;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-05 17:04
 **/
public class 数组 {

    public static void main(String[] args) {
//        数组声明Test();
        数组的拷贝Test();
    }

    private static void 数组声明Test() {
        int [] arr = new int [10];
        System.out.println(arr);
        Integer [] arr1 = new Integer [10];
        System.out.println(arr1);
        String [] arr2 = new String [10];
        System.out.println(arr2);
        Boolean [] arr3 = new Boolean [10];
        System.out.println(arr3);
        System.out.println(arr3.length);
        System.out.println(arr3[0]);
        boolean [] arr4 = new boolean [10];
        System.out.println(arr4);
        double [] arr5 = new double [10];
        System.out.println(arr5);
        char [] arr6 = new char [10];
        System.out.println(arr6);
        System.out.println(arr6[0]);
    }

    private static void 数组的拷贝Test() {
        String [] arr = new String [] { "1", "2", "3" };
        String [] arr1 = arr;
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr1));

        arr[0] = "3";
        System.out.println("---------------------");

        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr1));

        System.out.println("========================");

        int [] arr3 = new int [] { 1, 2, 3};
        int [] arr4 = arr3;
        System.out.println(Arrays.toString(arr3));
        System.out.println(Arrays.toString(arr4));

        arr3[0] = 3;
        arr4[2] = 1;

        System.out.println("---------------------");

        System.out.println(Arrays.toString(arr3));
        System.out.println(Arrays.toString(arr4));

        System.out.println("========================");

        String [] arr5 = new String[] { "a", "b", "c", "d", "e" };
        String [] arr6 = Arrays.copyOf(arr5, 1);
        String [] arr7 = Arrays.copyOf(arr5, 10);

        arr5[0] = "e";

        System.out.println(Arrays.toString(arr5));
        System.out.println(Arrays.toString(arr6));
        System.out.println(Arrays.toString(arr7));
    }
}