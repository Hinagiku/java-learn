package com.zhao.第三章;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-05 16:16
 **/
public class 构建字符串 {

    public static void main(String[] args) {
//        insert测试();
        delete测试();
    }

    private static void insert测试() {
        StringBuilder strB = new StringBuilder("abcde");
        System.out.println(strB.insert(1, 1).toString());
        System.out.println(strB.insert(6, 1).toString());
        System.out.println(strB.insert(8, 1).toString());
    }

    private static void delete测试() {
        StringBuilder strB = new StringBuilder("abcdefg");
        System.out.println(strB.delete(0, 1).toString());
        System.out.println(strB.delete(3, 5).toString());
        System.out.println(strB.delete(3, 7).toString());
        System.out.println(strB.delete(3, 7).toString());
    }
}
