package com.zhao.第五章.model1;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-23 17:08
 **/
public abstract class Person {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract String getDescription();

    public Person(String name) {
        this.name = name;
    }
}
