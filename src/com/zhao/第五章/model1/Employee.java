package com.zhao.第五章.model1;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-23 17:27
 **/
public class Employee extends Person {

    private Double salary;

    private Integer age;

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String getDescription() {
        return String.format("a employee with a salary of $%.2f", salary);
    }

    public Employee(String name, Double salary, Integer age) {
        super(name);
        this.salary = salary;
        this.age = age;
    }
}
