package com.zhao.第五章.model1;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-23 17:13
 **/
public class Student extends Person {

    private String major;

    public String getMajor() {
        return major;
    }

    public Student(String name, String major) {
        super(name);
        this.major = major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    @Override
    public String getDescription() {
        return "a student majoring in " + major;
    }
}
