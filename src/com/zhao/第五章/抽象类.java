package com.zhao.第五章;

import com.zhao.第五章.model1.Employee;
import com.zhao.第五章.model1.Person;
import com.zhao.第五章.model1.Student;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-23 16:55
 **/
public class 抽象类 {

    public static void main(String[] args) {
        抽象类子类实例化Test();
    }

    public static void 抽象类子类实例化Test() {
        Person [] people = new Person[2];
        people[0] = new Employee("李四", 8000D, 18);
        people[1] = new Student("王五", "计算机");
        for (Person person : people) {
            System.out.println(person.getDescription());
        }
    }
}
