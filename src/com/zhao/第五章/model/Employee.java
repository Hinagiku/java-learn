package com.zhao.第五章.model;

import java.time.LocalDate;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-17 15:04
 **/
public class Employee {

    private String name;

    private Double salary;

    private LocalDate hireDay;

    public Employee() {
    }

    public Employee(String name, Double salary, Integer year, Integer month, Integer day) {
        this.name = name;
        this.salary = salary;
        this.hireDay = LocalDate.of(year, month, day);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public LocalDate getHireDay() {
        return hireDay;
    }

    public void setHireDay(LocalDate hireDay) {
        this.hireDay = hireDay;
    }

    public void raiseSalary(Double byPercent) {
        Double raise = salary * byPercent / 100;
        salary += raise;
    }

    public Employee getBuddy() {
        return null;
    }
}
