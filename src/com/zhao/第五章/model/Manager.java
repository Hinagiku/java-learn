package com.zhao.第五章.model;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-17 15:12
 **/
public class Manager extends Employee {

    private Double bonus;

    public Manager() {
    }

    public Manager(String name, Double salary, Integer year, Integer month, Integer day) {
        super(name, salary, year, month, day);
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    @Override
    public Double getSalary() {
        Double salary = super.getSalary();
        return bonus + salary;
    }

    @Override
    public Manager getBuddy() {
        return null;
    }

//    @Override
//    public Integer getBuddy() {
//        return null;
//    }
}
