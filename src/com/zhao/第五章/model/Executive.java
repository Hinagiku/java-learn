package com.zhao.第五章.model;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-17 15:48
 **/
public class Executive extends Manager {

    private Double equity;

    public Executive(String name, Double salary, Integer year, Integer month, Integer day, Double equity) {
        super(name, salary, year, month, day);
        this.equity = equity;
    }

    public Double getEquity() {
        return equity;
    }

    public void setEquity(Double equity) {
        this.equity = equity;
    }
}
