package com.zhao.第五章;

import com.zhao.第五章.model.Employee;
import com.zhao.第五章.model.Executive;
import com.zhao.第五章.model.Manager;

/**
 * @program: java-learn
 * @description:
 * @author: zzg
 * @create: 2020-06-17 15:03
 **/
public class 多态 {

    public static void main(String[] args) {
//        子类和超类的转换();
//        子类和超类的数组转换Test();
        超类和子类的相互转换Test();
    }

    public static void 子类和超类的转换Test() {
        Manager boss = new Manager("李四", 8000D, 1987, 12, 15);
        boss.setBonus(5000D);

        Employee [] employees = new Employee[3];
        employees[0] = boss;
        employees[1] = new Employee("王二", 5000D, 1991, 10, 24);
        employees[2] = new Employee("张三", 5000D, 1994, 9, 24);

        Manager tempManager = (Manager) employees[0];
        System.out.println(tempManager.getBonus());
        for (Employee employee : employees) {
            System.out.println("name=" + employee.getName() + ",salary=" + employee.getSalary());
        }

        Executive executive = new Executive("陈五", 10000D, 1992, 12, 13, 10D);
        System.out.println(executive.getName());
    }

    public static void 子类和超类的数组转换Test() {
        Manager [] managers = new Manager[1];
        managers[0] = new Manager("李四", 8000D, 1987, 12, 15);
        System.out.println(managers[0].getName());
        Employee [] employees = managers;
        employees[0] = new Employee("张三", 5000D, 1994, 9, 24);
        System.out.println(employees[0].getName());
    }

    public static void 方法重载返回可协变类型() {
        Manager manager = new Manager("李四", 8000D, 1987, 12, 15);
        Employee employee = new Employee("张三", 5000D, 1994, 9, 24);
        Employee tempEmployee = employee.getBuddy();
        Manager tempManager = manager.getBuddy();
    }

    public static void 超类和子类的相互转换Test() {
        try {
            Employee employee = new Employee();
            Manager manager = (Manager) employee;
        } catch (Exception e) {
            System.out.println("Employee > Manager Error");
        }
        try {
            Manager manager = new Manager();
            manager.setBonus(1000D);
            Employee employee = manager;
            Manager manager1 = (Manager) employee;
            System.out.println(manager1.getBonus());
        } catch (Exception e) {
            System.out.println("Manager > Employee Error");
            System.out.println("Employee > Manager Error");
        }

        try {
            Manager manager = new Manager();
            manager.setBonus(1000D);
            Employee employee = (Employee) manager;
            Manager manager1 = (Manager) employee;
            System.out.println(manager1.getBonus());
        } catch (Exception e) {
            System.out.println("Manager > Employee Error");
            System.out.println("Employee > Manager Error");
        }
    }
}
